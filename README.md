# JSPoolPerformanceTesting
A quick check of JSPool performance in C#

It would appear that one engine running one function 200k times without being disposed takes about 1.5s on my laptop. Disposing it after each run takes about 15s, or 10x slower.
