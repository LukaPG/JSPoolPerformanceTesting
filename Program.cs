﻿#define MEASURING

using JavaScriptEngineSwitcher.Core;
using JavaScriptEngineSwitcher.V8;
using JSPool;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace JSPoolPerformanceExploration
{
    class Program
    {
        private const int numberOfIterations = 2_000_000;

        private static readonly string _script = @"
function rule(amount) {
    if (amount <= 5000) return 1;
    if (amount <= 10000) return 2;
    return 3;
}";

        private static readonly Random _random = new Random();

        private static List<int> RandomNumbers
        {
            get
            {
                var randoms = new List<int>();
                foreach (var i in Enumerable.Range(0, numberOfIterations))
                {
                    randoms.Add(_random.Next(12000));
                }
                return randoms;
            }
        }

        static void Main()
        {
            // Initialize pool
            var pool = GetPoolOfV8Engines();

#if MEASURING
            var sw = new Stopwatch();

            sw.Start();
            // First run
            DoWorkInJSEngine(pool);
            sw.Stop();
            var e = sw.Elapsed;

            var elapsedTime = string.Format("{0:00}:{1:00}.{2:00}",
                e.Minutes, e.Seconds, e.Milliseconds / 10);

            Console.WriteLine($"Calling into JS {numberOfIterations} times took {elapsedTime}");
#else
            DoWorkInJSEngine(pool);
#endif
        }

        static JsPool GetPoolOfV8Engines()
        {
            IJsEngineSwitcher engineSwitcher = JsEngineSwitcher.Current;

            engineSwitcher.EngineFactories.AddV8();
            engineSwitcher.DefaultEngineName = V8JsEngine.EngineName;

            return new JsPool(new JsPoolConfig
            {
                Initializer = initEngine => initEngine.Execute(_script),
                MaxUsagesPerEngine = 0
            });
        }

        static List<int> DoWorkInJSEngine(JsPool pool)
        {
            // First run
            var results = new List<int>();
            using (var engine = pool.GetEngine())
            {
                foreach (var amount in RandomNumbers)
                {
                    results.Add(engine.CallFunction<int>("rule", amount));
                }
            }
            return results;
        }
    }
}
